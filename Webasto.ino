#include "LiquidCrystal_I2C.h"
int outTX = 16;                 // digital pin 0
int led = 5;
LiquidCrystal_I2C lcd(63, 20, 4);

void setup() {
	Serial.begin(115200);
	Serial.println("SETUP");
	lcd.begin(4, 0);
	lcd.backlight();
	lcd.setCursor(0, 1);
	lcd.print("WEBASTO v1.0");
	pinMode(led, OUTPUT);
	digitalWrite(outTX, LOW);
	pinMode(outTX, OUTPUT);              // ���� ��� �����
	digitalWrite(outTX, HIGH);
}

void loop() {
	delay(10000);
	run_webasto();

}

void run_webasto() //�������� F4 03 20 3B EC - �������� 59 ����� (3B=3*16+11=48+11)
{
	digitalWrite(led, LOW);
	digitalWrite(outTX, LOW);
	delayMicroseconds(1240);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(440);
	digitalWrite(outTX, LOW);
	delayMicroseconds(400);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(2520);
	digitalWrite(outTX, LOW);
	delayMicroseconds(400);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(840);
	digitalWrite(outTX, LOW);
	delayMicroseconds(2920);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(400);
	digitalWrite(outTX, LOW);
	delayMicroseconds(2480);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(440);
	digitalWrite(outTX, LOW);
	delayMicroseconds(840);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(800);
	digitalWrite(outTX, LOW);
	delayMicroseconds(440);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(840);
	digitalWrite(outTX, LOW);
	delayMicroseconds(400);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(1240);
	digitalWrite(outTX, LOW);
	delayMicroseconds(840);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(840);
	digitalWrite(outTX, LOW);
	delayMicroseconds(1240);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(840);
	digitalWrite(outTX, LOW);
	delayMicroseconds(400);
	digitalWrite(outTX, HIGH);
	delayMicroseconds(320);
	digitalWrite(led, HIGH);
}

